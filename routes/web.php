<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PublicController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', [PublicController::class, 'index'])->name('home');

Route::prefix('/admin')->group(function(){

    Route::get('/', [AdminController::class, 'index'])->name('admin_home');
    
    Route::get('/profile/{id?}', [AdminController::class, 'profile'])->name('admin_profile');
    Route::post('/profile/{id}/save', [AdminController::class, 'profile_save'])->name('admin_profile_save'); // << ÚJ meglepi
        
});