<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Laravel - Lesson 1

Laravel ismerkedés, bezető első alkalom. HTTP kérés, webes környezet müködése. MVC szemlélet alapok, modulok ismertetése.

Érintett témakörök:
 - Routing (POST, GET, PREFIX, named routes, params)
 - Views (Blade, {{}},)
 - Controllers 
 - Request (input[GET] )
 - Response (code, json)
