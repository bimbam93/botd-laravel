<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    
    public function index(){

        $user = "Kis Tamás";

        return view(
            'admin.home',
            [
                "user"=>$user,
                "role"=>"AtyaÚRISTEN"
                ]
        );

    }

    public function profile($id = 0){

        return view('admin.profile', ['id'=>$id]);

    }
    
    public function profile_save($id = 0, Request $req){

        if($id=='a'){
            abort(500);
        }

        //dd($req);
        return response()->json([
            'name' => $req->input('name'),
            'age' => $req->input('age')
        ]);
        
        
    }



}
